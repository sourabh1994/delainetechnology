package com.example.delainetech;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class CatRecylerAdapter extends RecyclerView.Adapter<CatRecylerAdapter.MyViewHolder> {
    @NonNull

    private List<CatRecyclerPojo> dataList;
    private Context context;

    public CatRecylerAdapter(@NonNull List<CatRecyclerPojo> dataList,Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cat_recycler_content, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        CatRecyclerPojo data = dataList.get(i);
        myViewHolder.illnessName.setText(data.getDescription());
        myViewHolder.relativeLayout.setBackgroundColor(data.getColor());
        Glide.with(context)
                .load(dataList.get(i).getImg())
                .into(myViewHolder.illnessImg);
        myViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CatRecyclerPojo catRecyclerPojo=dataList.get(i);
                Bundle bundle=new Bundle();
                Gson gson = new Gson();
                Intent intent =new Intent(context,ConsultationActivity.class);
                bundle.putString("data", gson.toJson(catRecyclerPojo));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView illnessName;
        public ImageView illnessImg;
        public RelativeLayout relativeLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            illnessName = (TextView) itemView.findViewById(R.id.illnessName);
            illnessImg = (ImageView) itemView.findViewById(R.id.illnessImg);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLy);
        }
        }
    }
