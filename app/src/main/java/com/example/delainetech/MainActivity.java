package com.example.delainetech;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {
    Unbinder unbinder;
    @BindView(R.id.onlineText)
    TextView onlineText;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.catLy)
    LinearLayout catLy;
    @BindView(R.id.recylerView)
    RecyclerView recylerView;
    @BindView(R.id.healthConcernTv)
    TextView healthConcernTv;
    @BindView(R.id.recylerViewHealthConcern)
    RecyclerView recylerViewHealthConcern;
    ArrayList<CatRecyclerPojo> catRecyclerPojos = new ArrayList<>();
    ArrayList<CatRecyclerPojo> srcRecyclerPojo = new ArrayList<>();
    CatRecylerAdapter catRecylerAdapter;
    HealthCatRecylerAdapter healthCatRecylerAdapter;
    @BindView(R.id.adultTv)
    TextView adultTv;
    @BindView(R.id.childrenTv)
    TextView childrenTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.LayoutManager healthLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        recylerView.setLayoutManager(layoutManager);
        recylerViewHealthConcern.setLayoutManager(healthLayoutManager);
        catRecylerAdapter = new CatRecylerAdapter(catRecyclerPojos, this);
        healthCatRecylerAdapter = new HealthCatRecylerAdapter(srcRecyclerPojo, this);
        recylerView.setAdapter(catRecylerAdapter);
        recylerViewHealthConcern.setAdapter(healthCatRecylerAdapter);
        setCategory();
        catRecylerAdapter.notifyDataSetChanged();
        setHealthConcern();
        healthCatRecylerAdapter.notifyDataSetChanged();


    }

    public void setCategory() {
        catRecyclerPojos.clear();
        catRecyclerPojos.add(new CatRecyclerPojo("Cough" + "\n" + "& Cold", R.drawable.cough, Color.RED));
        catRecyclerPojos.add(new CatRecyclerPojo("Heart" + "\n" + "Disease", R.drawable.cough, Color.GRAY));
        catRecyclerPojos.add(new CatRecyclerPojo("Cough" + "\n" + "& Cold", R.drawable.cough, Color.MAGENTA));
        catRecyclerPojos.add(new CatRecyclerPojo("Cough" + "\n" + "& Cold", R.drawable.cough, Color.RED));
        catRecyclerPojos.add(new CatRecyclerPojo("Heart" + "\n" + "Disease", R.drawable.cough, Color.GRAY));
        catRecyclerPojos.add(new CatRecyclerPojo("Cough" + "\n" + "& Cold", R.drawable.cough, Color.MAGENTA));
    }

    public void setHealthConcern() {
        srcRecyclerPojo.clear();
        srcRecyclerPojo.add(new CatRecyclerPojo("Dental"+"\n" +"Care", R.drawable.dental,Color.RED));
        srcRecyclerPojo.add(new CatRecyclerPojo("General"+"\n"+"Doctor", R.drawable.dental,Color.GRAY));
        srcRecyclerPojo.add(new CatRecyclerPojo("Vision"+"\n"+ "Care", R.drawable.dental,Color.MAGENTA));
    }

    @OnClick({R.id.adultTv, R.id.childrenTv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.adultTv:
                adultTv.setTextColor(Color.WHITE);
                adultTv.setBackground(getResources().getDrawable(R.drawable.selected_backgroud));
                childrenTv.setTextColor(Color.GRAY);
                childrenTv.setBackground(getResources().getDrawable(R.drawable.button_color_rounded_corners));
                break;
            case R.id.childrenTv:
                childrenTv.setTextColor(Color.WHITE);
                childrenTv.setBackground(getResources().getDrawable(R.drawable.selected_backgroud));
                adultTv.setTextColor(Color.GRAY);
                adultTv.setBackground(getResources().getDrawable(R.drawable.button_color_rounded_corners));
                break;
        }
    }
}
