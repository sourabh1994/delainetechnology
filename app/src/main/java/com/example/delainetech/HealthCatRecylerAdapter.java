package com.example.delainetech;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class HealthCatRecylerAdapter extends RecyclerView.Adapter<HealthCatRecylerAdapter.MyViewHolder> {
    @NonNull

    private List<CatRecyclerPojo> dataList;
    private Context context;

    public HealthCatRecylerAdapter(@NonNull List<CatRecyclerPojo> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.srch_health_rec_content, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        CatRecyclerPojo data = dataList.get(i);
        myViewHolder.illnessName.setText(data.getDescription());

        Glide.with(context)
                .load(dataList.get(i).getImg())
                .into(myViewHolder.illnessImg);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView illnessName;
        public ImageView illnessImg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            illnessName = (TextView) itemView.findViewById(R.id.illnessName);
            illnessImg = (ImageView) itemView.findViewById(R.id.illnessImg);
        }
    }
}
