package com.example.delainetech;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

public class CatRecyclerPojo {
    @SerializedName("description")
    private String description;
    @SerializedName("img")
    int img;
    @SerializedName("color")
    int color;

    public CatRecyclerPojo(String description, int img,int color) {
        this.description = description;
        this.img = img;
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
