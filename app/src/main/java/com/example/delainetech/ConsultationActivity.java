package com.example.delainetech;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ConsultationActivity extends AppCompatActivity {
    Bundle bundle;
    CatRecyclerPojo catRecyclerPojo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation);
        bundle=getIntent().getExtras();
        Gson gson = new Gson();
        String data = bundle.getString("data");
        catRecyclerPojo=new Gson().fromJson(bundle.getString("data"), CatRecyclerPojo.class);
          Toast.makeText(ConsultationActivity.this, catRecyclerPojo.getDescription(),Toast.LENGTH_LONG).show();
    }
}
